import numpy as np

# Function for calculating the gaussian probability
def calculateProbability(x, mean, stdev):
	exponent = math.exp(-(math.pow(x-mean,2)/(2*math.pow(stdev,2))))
	return (1 / (math.sqrt(2*math.pi) * stdev)) * exponent

# Function for finding mean of array
def mean(numbers):
	return numbers.mean()
 
# Function for finding standard deviation of array 
def stdev(numbers):
	return numbers.std()

# Function for calculating mean and standard deviation of every attribute
def getAttribute(dataset):
	attributes = [(mean(dataset[:,i]), stdev(dataset[:,i])) for i in range(0,len(dataset[0]))]
	del attributes[-1]
	return attributes

# Function for making a class map which have all the dataset separated
def getClassMap(dataset):
	classMap = {}
	for i in range(0,len(dataset)):
		vector = dataset[i]
		if (vector[-1] not in classMap):
			classMap[vector[-1]] = []
		classMap[vector[-1]].append(vector)
	return classMap

# Function for summarizing the data by class
def summarizeByClass(dataset):
	separated = separateByClass(dataset)
	summaries = {}
	for classValue, instances in separated.iteritems():
		summaries[classValue] = getAttribute(instances)
	return summaries

# Function for calculating probabilities of all classes for a given inputvector
def calculateClassProbabilities(summaries, inputVector):
	probabilities = {}
	for classValue, classSummaries in summaries.iteritems():
		probabilities[classValue] = 1
		for i in range(len(classSummaries)):
			mean, stdev = classSummaries[i]
			x = inputVector[i]
			probabilities[classValue] *= calculateProbability(x, mean, stdev)
	return probabilities

# Function for predicting a label for a input vector
def getPredict(summaries, inputVector):
	probabilities = calculateClassProbabilities(summaries, inputVector)
	bestLabel, bestProb = None, -1
	for classValue, probability in probabilities.iteritems():
		if bestLabel is None or probability > bestProb:
			bestProb = probability
			bestLabel = classValue
	return bestLabel	

# Function to calculate the accuracies for a test data
def getAccuracy(summaries,testdata):
	correct=0.0
	for i in range(0,len(testdata)):
		if(getPredict(summaries,testdata[i])==testdata[i][-1]):
			correct=correct+1.0
	acc=correct/float(len(testdata))
	return acc		

# Function to calculate 10 fold validation and getting the mean accuracy
def getTenFold(dataset):
	acc=0.0
	for i in range(0,len(dataset)):
		#join the dataset
		training = list()
		testdata = list()
		summaries = summarizeByClass(training,testdata)
		acc=acc+getAccuracy(summaries,testdata)
	return (acc/10.0)

# Function to calculate the mean accuracy by shuffling the data 
def getMeanAccuracy(dataset):
	meanacc=0.0
	for i in range(0,30):
		#break the dataset into 10 pieces
		meanacc=meanacc+getTenFold(dataset)
		shuffle(dataset)
	return (meanacc/30.0) 

def loadData(filename):
	dataset = []
	# code for loading a data
	return dataset

def bayes():
	#dataset=loadData("")
	dataset = np.array([1,20,0])
	dataset = np.vstack([dataset,[2,21,1]])
	dataset = np.vstack([dataset,[3,22,0]])
	dataset = np.vstack([dataset,[3,34,1]])
	dataset = np.vstack([dataset,[2,23,0]])
	dataset = np.vstack([dataset,[2,11,1]])
	dataset = np.vstack([dataset,[3,42,0]])
	dataset = np.vstack([dataset,[4,34,0]])
	dataset = np.vstack([dataset,[2,26,1]])
	dataset = np.vstack([dataset,[3,45,0]])
	accuracy=getMeanAccuracy(dataset)
	print "Accuracy = "+str(getMeanAccuracy(dataset)) 

def mainmethod()
	dataset = np.array([1,20,0])
	dataset = np.vstack([dataset,[2,21,1]])
	dataset = np.vstack([dataset,[3,22,0]])
	test=[[3,34,1],[2,23,0]]
	summaries = summarizeByClass(dataset)
	#print('Attribute summaries: {0}').format(summary)
	#separated = getClassMap(dataset)
	#print('Separated instances: {0}').format(separated)

mainmethod():	