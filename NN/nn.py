'''
Created on 23-Mar-2017

@author: rohit
'''
import numpy as np
from random import seed
from random import random
 
# Initialize a network
# Neural network consist of 3 layers
def assignWts(n_inputs, n_hidden, n_outputs):
    network = list()
    #Hidden Layer weights
    hidden_layer = [{'weights':[random() for _ in range(n_inputs + 1)]} for _ in range(n_hidden)]
    network.append(hidden_layer)
    #Output Layer weights
    output_layer = [{'weights':[random() for _ in range(n_hidden + 1)]} for _ in range(n_outputs)]
    network.append(output_layer)
    return network

# Function for fetching the weighted sum 
def getWeightedSum(weights,inputs):
    result=weights[-1]
    for i in range(len(weights)-1):
        result+= weights[i]*inputs[i]
    return result

# Applying the activation function in our case it sigmoid function
def transfer(activation):
    return 1.0 / (1.0 + np.exp(-activation))

# Function for taking derivative of neuron output
def transfer_derivative(output):
    return output * (1.0 - output)

# Function back propogation 
def backward_propagate_error(network, expected):
    for i in reversed(range(len(network))):
        layer = network[i]
        errors = list()
        if i != len(network)-1:
            for j in range(len(layer)):
                error = 0.0
                for neuron in network[i + 1]:
                    error += (neuron['weights'][j] * neuron['delta'])
                errors.append(error)
        else:
            for j in range(len(layer)):
                neuron = layer[j]
                errors.append((expected[j] - neuron['output'])*(-1))
        #print "layer "+str(i)
        for j in range(len(layer)):
            neuron = layer[j]
            neuron['delta'] = errors[j] * transfer_derivative(neuron['output'])
            #print neuron['delta']

# Function for feed forward propogation
def forward_propogate(network,inputs):
    for layer in network:
        outputs=[]
        for neuron in layer:
            activation=getWeightedSum(neuron['weights'],inputs)
            neuron['output']=transfer(activation)
            outputs.append(neuron['output'])
        inputs=outputs    
    return inputs

# Update network weights with error
def update_weights(network, row, l_rate):
    for i in range(len(network)):
        inputs = row[:-1]
        if i != 0:
            inputs = [neuron['output'] for neuron in network[i - 1]]
        for neuron in network[i]:
            for j in range(len(inputs)):
                neuron['weights'][j] -= l_rate * neuron['delta']*inputs[j] 
            neuron['weights'][-1] -= l_rate * neuron['delta']
           
# Train a network for a fixed number of epochs
def train_network(network, train, l_rate, n_epoch, n_outputs,y):
    for epoch in range(n_epoch):
        sum_error = 0
        for i in range(len(train)):
            #print "input "+str(i)
            outputs = forward_propogate(network, train[i])
            expected = [0 for _ in range(n_outputs)]
            expected[int(y[i])-1] = 1
            sum_error += sum([(expected[j]-outputs[j])**2 for j in range(len(expected))])
            backward_propagate_error(network, expected)
            update_weights(network, train[i], l_rate)
        print('>epoch=%d, lrate=%.3f, error=%.3f' % (epoch, l_rate, sum_error))            
    return network    
    
# Function for training the neural network
def trainNeuralNet(h,x,y):
    seed(1)
    network = assignWts(64,h, 3)
    network=train_network(network, x, 0.5, 100,3,y)
    return network
    
def getAccuracy(network,test,y):
    correct=0.0
    for i in range(len(test)):
        outputs=forward_propogate(network,test[i])
        if(outputs.index(max(outputs))==(y[i]-1)):
            correct=correct+1
    return (correct/(len(test)*1.0))        

def getData(filename):
    f=open(filename)
    pts=np.empty([1, 65])

    for line in f:
        line=line[:-1]
        nums=line.split(",")    
        if(nums[-1]=='1' or nums[-1]=='2' or nums[-1]=='3'):
            cols=[]
            for r in nums:
                cols.append(float(r))    
            t=np.asarray(cols)
            pts=np.vstack([pts,t])

    pts=pts[1:]
    return pts    

def normalize(x):
    m=np.mean(x,0)
    for i in range(len(x)):
        x[i]-=m
    s=np.std(x,0)
    s[s==0]=1
    for i in range(len(x)):
        x[i]/=s
        x[i]/=3
    return x
        
def neural():
    tra=getData("optdigits.tra")
    network=trainNeuralNet(10,normalize(tra[:,:-1]),tra[:,-1])
    tes=getData("optdigits.tes")
    print "Accuracy = "+str(getAccuracy(network,tes[:,:-1],tes[:,-1]))
    
neural()            