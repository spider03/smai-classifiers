'''
Created on 21-Mar-2017

@author: rohit
'''
import sys
import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm, datasets
from random import shuffle
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import ShuffleSplit            

def plotSVM(pts,c,d,kernelFunc):
    x=pts[:,:2]
    y=pts[:,-1]
    h=.02
    clf = svm.SVC(kernel=kernelFunc, gamma=d, C=c).fit(x, y)
    x_min, x_max = x[:, 0].min() - 1, x[:, 0].max() + 1
    y_min, y_max = x[:, 1].min() - 1, x[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),np.arange(y_min, y_max, h))
    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])

    # Put the result into a color plot
    Z = Z.reshape(xx.shape)
    plt.contourf(xx, yy, Z, cmap=plt.cm.coolwarm, alpha=0.8)

    # Plot also the training points
    plt.scatter(x[:, 0], x[:, 1], c=y, cmap=plt.cm.coolwarm)
    plt.xlabel('Sepal length')
    plt.ylabel('Sepal width')
    plt.xlim(xx.min(), xx.max())
    plt.ylim(yy.min(), yy.max())
    plt.xticks(())
    plt.yticks(())
    plt.show()

def getMeanAccuracy(c,d,kernelFunc,x,y,details):
    clf=svm.SVC(C=c, kernel=kernelFunc, gamma=d)
    cv = ShuffleSplit(n_splits=10, test_size=0.1, random_state=0)
    scores = cross_val_score(clf, x,y, cv=cv)
    details[0]=details[0]+scores.mean()
    details[1]=details[1]+scores.std()
    #print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
    #return scores.mean()

def getAccForCD(c,d,kernelFunc,x,y,details):
    sumacc=0.0
    for _ in range(0,30):    
        getMeanAccuracy(c,d,kernelFunc,x,y,details)
        #shuffle(pts)
    details[0]=details[0]/30.0
    details[1]=details[1]/30.0
    #return (sumacc/(30*1.0))    

def classifySVM():
    f=open("Data_SVM.csv")
    f2=open("poly.csv",'w')    
    kernelFunc='rbf'
    pts=np.empty([1, 3])
    i=0
    for line in f:
        if(i!=0):
            line=line[:-1]
            nums=line.split(",")    
            cols=[]
            for r in nums:
                cols.append(float(r))
            x=np.asarray(cols)
            pts=np.vstack([pts,x])  
        i=i+1        
    pts=pts[1:]   
    x=pts[:,:2]
    y=pts[:,-1]  
    c=0.1 
    cmax=0.0
    dmax=0.0
    amax=0.0
    while (c<1001):
        for d in range(1,11,1):
           details=[0.0,0.0]
           getAccForCD(c*1.0,d,kernelFunc,x,y,details)
           f2.write(str(c)+","+str(d)+","+str(details[0])+","+str(details[1])+"\n")
           if(details[0]>amax):
                amax=details[0]
                cmax=c
                dmax=d
           print "For c="+str(c)+" d="+str(d)+" Accuracy="+str(details[0])
           #sys.stdout.flush()
           f2.flush()
        c=c*10.0

    print "Highest accuracy for c="+str(cmax)+" d="+str(dmax)
    plotSVM(pts,cmax,dmax,kernelFunc)

classifySVM()                                        