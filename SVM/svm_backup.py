'''
Created on 21-Mar-2017

@author: rohit
'''

from sklearn import svm
from random import shuffle

def splitData(pts):
    size=len(pts)/10
    end=size
    start=0
    dataPts=[]
    for _ in range(0,10):
        x=[]
        y=[]
        for i in range(start,end):
            x.append(pts[i][:-1])
            y.append(pts[i][-1])
        temp=[]
        temp.append(x)
        temp.append(y)
        dataPts.append(temp)
        start=end
        end=end+size
    return dataPts

def getAccuracy(c,d,kernelFunc,dataPts,index):
    clf=svm.SVC(C=c, kernel=kernelFunc, degree=d)
    for i in range(0,10):
        if(i!=index):
            clf=clf.fit(dataPts[i][0],dataPts[i][1])
    return clf.score(dataPts[index][0],dataPts[index][1])        


def getMeanAccuracy(c,d,kernelFunc,pts):
    dataPts=splitData(pts)
    sumacc=0.0
    for i in range(0,10):
        sumacc=sumacc+getAccuracy(c,d,kernelFunc,dataPts,i)
    return (sumacc/(10*1.0))

def getAccForCD(c,d,kernelFunc,pts):
    sumacc=0.0
    for _ in range(0,30):
        sumacc=sumacc+getMeanAccuracy(c,d,kernelFunc,pts)
        shuffle(pts)
    return (sumacc/(30*1.0))    

def classifySVM():
    f=open("Data_SVM.csv")
    f2=open("poly.csv",'w')
    pts=[]
    kernelFunc='poly'
    i=0
    for line in f:
        if(i!=0):
            line=line[:-1]
            cols=line.split(",")    
            pts.append(cols)
        i=i+1        

    
    for c in range(1,101,1):
        for d in range(1,101,1):
           res=getAccForCD(c*1.0,d,kernelFunc,pts)
           f2.write(str(c)+","+str(d)+","+str(res)+"\n")
           print "For c="+str(c)+" d="+str(d)+" Accuracy="+str(res)

classifySVM()                                        